$.ajax({
    url: "https://swapi.co/api/people/",
    method: "GET",
    success: function(data) {
        for (let i = 1; i < 10; i++ ) {
            $.get(`https://swapi.co/api/people/?page=${i}`, function(dataN) {
                dataN.results.forEach(item => {
                    $.get(item.homeworld, function(dat) {
                        if (item.starships.length > 0) {
                            $("#sw-people").append(`<li>Имя: ${item.name}, пол: ${item.gender}, родная планета: ${dat.name}.<br>
<button class="starships-btn" data-url="${item.starships}">Список кораблей </button></li>`);
                        } else {
                            $("#sw-people").append(`<li>Имя: ${item.name}, пол: ${item.gender}, родная планета: ${dat.name}.</li>`);
                        }
                    });
                }, "json");
            })
        }
    },
    error: function(data){
        console.log(data)
    }
});

const container = document.querySelector(".container");
container.addEventListener("click", (e) => {
    if (e.target.className !== "starships-btn") return;
    const starshipsBtn = e.target.closest('.starships-btn');
    const li = e.target.closest("LI");
    const url = starshipsBtn.dataset.url;
    const urls = url.split(",");
    starshipsBtn.remove();
    const h3 = document.createElement("h3");
    h3.innerText = "Пилотируемые корабли:";
    li.append(h3);
    urls.forEach(item => {
        $.get(item, function(data) {
            li.append(`${data.name}! `)
        })
    })

});
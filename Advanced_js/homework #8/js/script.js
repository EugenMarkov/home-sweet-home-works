async function showIP(urlIP, urlIPAPI, container){
    const answerIP = await fetch(urlIP);
    const result = await answerIP.json();
    // console.log(result);
    const fields = "?fields=1572889";
    const lang = "lang=ru";
    const params = fields + "&" + lang;
    // console.log(params);
    const urlWithParams = urlIPAPI + result.ip + params;

    const answerAdress = await fetch(urlWithParams);
    const resultAdress = await answerAdress.json();
    // console.log(resultAdress);

    const resultAdressArr = Object.entries(resultAdress);
    // console.log(resultAdressArr);

    resultAdressArr.forEach(item => {
        const li = document.createElement("li");
        li.textContent = `${item[0]}:  ${item[1]}`;
        container.append(li);
    });
}

const showIPBtn = document.getElementById("ip-btn");

showIPBtn.addEventListener("click", ()=> {
    showIPBtn.disabled = true;
    showIP("https://api.ipify.org/?format=json",  "http://ip-api.com/json/", document.getElementById("list"));
});

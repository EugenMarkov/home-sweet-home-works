/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

function Hamburger(size, stuffing) {
    try {
        if (!size) {
            throw new HamburgerException("no size given");
        }
        this.size = size.size;

        if (!stuffing){
            throw new HamburgerException("no stuffing given");
        }
        this.stuffing = stuffing.name ;

        if (!(size.size === "small" || size.size === "large")) {
            throw new HamburgerException("invalid size");
        }
        if (!(stuffing.name === "cheese" || stuffing.name === "salad" || stuffing.name === "potato")) {
            throw new HamburgerException("invalid stuffing");
        }
        this.price = size.price + stuffing.price;
        this.calories = size.calories + stuffing.calories;
        this.topping = [];

    } catch (e) {
        return HamburgerException.message;
    }
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {size: "small", price: 50, calories: 20};
Hamburger.SIZE_LARGE = {size: "large", price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {name: "cheese", price: 10, calories: 20};
Hamburger.STUFFING_SALAD = {name: "salad", price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {name: "potato", price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {name: "mayo", price: 20, calories: 5};
Hamburger.TOPPING_SPICE = {name: "spice", price: 15, calories: 0};


/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */

Hamburger.prototype.addTopping = function (topping) {
    try {
        if (!(topping.name === "mayo" || topping.name === "spice")) {
            throw new HamburgerException("invalid topping");
        } else if (this.topping.find( function(ingredient) {
            return ingredient === topping
        })) {
            throw new HamburgerException("duplicate topping " +  topping.name);
        } else {
            this.topping.push(topping);
            this.price += topping.price;
            this.calories += topping.calories;
        }
    } catch (e) {
        return HamburgerException.message;
    }

};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */

Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (this.topping.length > 0) {
            var toppingN = this.topping.find( function(ingredient) {
                return ingredient === topping
            });
            var i = this.topping.indexOf(toppingN);
            this.topping.splice(i, 1);
            this.price -= topping.price;
            this.calories -= topping.calories;
        } else {
            throw new HamburgerException("no toppings")
        }
    } catch (e) {
        return HamburgerException.message;
    }
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */

Hamburger.prototype.getToppings = function (){
    return this.topping
};

/**
 * Узнать размер гамбургера
 */

Hamburger.prototype.getSize = function (){
    return this.size
};

/**
 * Узнать начинку гамбургера
 */

Hamburger.prototype.getStuffing = function (){
    return this.stuffing
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */

Hamburger.prototype.calculatePrice = function (){
    return this.price
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */

Hamburger.prototype.calculateCalories = function (){
    return this.calories
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */

function HamburgerException(message){
    this.name = 'HamburgerException';
    this.message = this.name + ": " + message;
    console.log(this.message);
}

// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with spice: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1


var h1 = new Hamburger();

var h2 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);

var h3 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.TOPPING_SPICE);

var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
h4.addTopping(Hamburger.TOPPING_MAYO);
h4.addTopping(Hamburger.TOPPING_SPICE);
h4.addTopping(Hamburger.TOPPING_MAYO);

console.log("Hamburger h4 is", h4.getSize());
console.log("Hamburger h4 has stuffing -", h4.getStuffing());
console.log("Hamburger h4 has toppings -", h4.getToppings()[0].name, "and", h4.getToppings()[1].name,);


const cells = document.querySelectorAll(".cell");
const computerCounter  = document.querySelector(".computer-count");
const playerCounter = document.querySelector(".player-count");
const winner = document.querySelector(".winner");

let playerScore = 0;
let computerScore = 0;

class Game {
    constructor(speed) {
        this.speed = speed;
        this.lastCell = undefined;
        playerScore = 0;
        computerScore = 0;
    }
    randomCell(list) {
        const index = Math.floor(Math.random()*list.length);
        const cell = list[index];
        if (cell === this.lastCell || cell.classList.contains("cell-green") || cell.classList.contains("cell-red")) {
            return this.randomCell(list);
        }
        this.lastCell = cell;
        return cell;
    }

    highlight() {
        const cell = this.randomCell(cells);
        cell.classList.add("cell-blue");
        setTimeout(() => {
            cell.classList.remove("cell-blue");
            cell.classList.add("cell-red");
            if (cell.classList.contains("cell-red")) {
                computerScore++;
            }
            if (cell.classList.contains("cell-green")) {
                computerScore--;
            }
            computerCounter.textContent = computerScore;
            if (playerScore < 50 && computerScore < 50) {
                this.highlight()
            }
            if (playerScore === 50) {
                winner.textContent = "Winner is Player";
            }
            if (computerScore === 50) {
                winner.textContent = "Winner is Computer";
            }
        }, this.speed);
    }
}

function catchCell(e) {
    if(!e.isTrusted) return;
    if (this.classList.contains("cell-blue")) {
        this.classList.remove('cell-blue');
        this.classList.add('cell-green');
        playerScore++;
    }
    playerCounter.textContent = playerScore;
}

cells.forEach((e)=> e.addEventListener("click", catchCell));

const easyGame = new Game(1500);
const mediumGame = new Game(1000);
const hardGame = new Game(500);

const easyLevel = document.getElementById("btn-easy");
const mediumLevel = document.getElementById("btn-medium");
const hardLevel = document.getElementById("btn-hard");
const newGame = document.getElementById("btn-new");


easyLevel.addEventListener("click", () => {
   lockLevelOfGame();
   easyGame.highlight();
} );

mediumLevel.addEventListener("click", () => {
    mediumGame.highlight();
    lockLevelOfGame();

} );
hardLevel.addEventListener("click", () => {
    hardGame.highlight();
    lockLevelOfGame();
} );

function lockLevelOfGame() {
    if (playerScore < 50 && computerScore < 50) {
        easyLevel.disabled = true;
        mediumLevel.disabled = true;
        hardLevel.disabled = true;
    }
}

newGame.addEventListener("click", () => {
    unlockLevelOfGame();
    playerScore = 0;
    computerScore = 0;
    winner.textContent = "";
    computerCounter.textContent = 0;
    playerCounter.textContent = 0;
    cells.forEach(el => {
        el.classList.remove("cell-blue");
        el.classList.remove("cell-red");
        el.classList.remove("cell-green");
    });
});

function unlockLevelOfGame() {
    if (playerScore === 50 || computerScore === 50){
        easyLevel.disabled = false;
        mediumLevel.disabled = false;
        hardLevel.disabled = false;
    }
}


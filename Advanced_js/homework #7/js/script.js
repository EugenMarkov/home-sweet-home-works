const ol = document.getElementById("sw-planets");

for (let i = 1; i < 8; i++) {
    let answer = fetch(`https://swapi.co/api/planets/?page=${i}`);
    answer.then(response => response.json()).then((result) => {
        return result.results.map( item => {
            const li = document.createElement("li");
            if (item.residents.length > 0) {
                li.innerHTML = `<div>Планета: ${item.name}. Климат: ${item.climate}. Преобладающая местность: ${item.terrain}</div>
<span>Резиденты: </span>`;
                li.setAttribute("data-url", `${item.residents}`);
            } else {
                li.innerHTML = `<div>Планета: ${item.name}. Климат: ${item.climate}.  Преобладающая местность: ${item.terrain}</div>`;
            }
            ol.append(li);
            return li;
        })
    }).then(result => {
        result.forEach(el => {
            if (el.getAttribute("data-url")) {
                const url = el.dataset.url;
                const residents = url.split(",");
                Promise.all([
                    residents.forEach(item => {
                        fetch(item).then(response => response.json())
                            .then(result => {
                                const resident = document.createElement("span");
                                resident.innerText = `${result.name}. `;
                                el.append(resident);
                            })
                    })
                ])
            }
        })
    })
}




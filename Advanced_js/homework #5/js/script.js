const xhr = new XMLHttpRequest();

xhr.open("GET", "https://swapi.co/api/films/");
xhr.send();
xhr.onload = function() {
    if (xhr.status !== 200) {
        console.log(`Ошибка ${xhr.status}: ${xhr.statusText}`);
    } else {
        const result = xhr.response;
        const resultArr = JSON.parse(result);
        const films = resultArr.results;
        const ul = document.getElementById("sw-films");

        ul.innerHTML = films.map(item => `<li>Episode: ${item.episode_id}
<h3>${item.title}</h3>
<p>${item.opening_crawl}</p>
<button class="characters-btn" data-id="${films.indexOf(item)}">Characters list</button>
</li>`).join('');

       const container = document.querySelector(".container");
        container.addEventListener("click", (e) => {
            if (e.target.className !== "characters-btn") return;
            const charsBtn = e.target.closest('.characters-btn');
            const li = e.target.closest("LI");
            const id = charsBtn.dataset.id;
            charsBtn.remove();

            const charsList = films[id].characters;

            charsList.forEach( el => {
                const charsXhr = new XMLHttpRequest();
                charsXhr.open("GET", el);
                charsXhr.send();
                charsXhr.onload = function() {
                    if ( charsXhr.status !== 200) {
                        console.log(`Ошибка ${ charsXhr.status}: ${ charsXhr.statusText}`);
                    } else {
                        const charsResult = charsXhr.response;
                        const charsInfo = JSON.parse(charsResult);
                        let p = document.createElement("p");
                        p.textContent = charsInfo.name;
                        li.append(p);
                    }
                };
                charsXhr.onerror = function() {
                    alert("Запрос не удался");
                };
            })
        })
    }
};
xhr.onerror = function() {
    alert("Запрос не удался");
};


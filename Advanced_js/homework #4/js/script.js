window.addEventListener( "load", () => {
    Column.createColumn();
});

class Column {
    constructor() {
        this.node = document.createElement('div');
        this.node.classList.add("column");

        this.addCard = document.createElement("button");
        this.addCard.classList.add("add-card-btn");
        this.addCard.innerText = "Add a card...";

        this.cardsNode = document.createElement('div');
        this.cardsNode.classList.add("cards");

        this.sortBtn = document.createElement("button");
        this.sortBtn.classList.add("sort-btn");
        this.sortBtn.innerText = "Sort cards";

        this.node.appendChild(this.sortBtn);
        this.node.appendChild(this.cardsNode);
        this.node.appendChild(this.addCard);
    }

    static createColumn() {
        const column = new Column();
        document.getElementById("container").append(column.node);
    }

    static createForm() {
        const form = document.createElement('form');
        form.innerHTML =
            '<div class="form-wrapper">' +
            '<textarea class="new-card-input"></textarea>' +
            '<input class="new-card-submit" type="submit" value="Add">' +
            '</div>';
        return form
    }
}

const container = document.getElementById("container");

container.addEventListener("click", (e) => {
    if (e.target.className !== "add-card-btn") return;
    const list = e.target.closest(".add-card-btn");
    const form = Column.createForm();
    list.before(form);
});

let index = 0;
container.addEventListener("click", (e) => {
    if (e.target.className !== "new-card-submit") return;
    e.preventDefault();
    const addBtn = e.target.closest(".new-card-submit");
    const form =  e.target.closest("FORM");
    const text = addBtn.previousSibling;
    const column = e.target.closest(".column");
    const sortBTN = column.firstChild;
    const cards = sortBTN.nextSibling;
    if (text.value !== "") {
        const card = document.createElement("div");
        card.classList.add("card");
        card.innerText = text.value;
        card.draggable = true;
        cards.append(card);
        form.remove();
        index++;
        card.id = `case${index}`;
        dragDrop();
    }
});


function allDrop(e) {
    e.preventDefault();
}

function drag(e) {
    e.dataTransfer.setData('text', e.target.id)
}

function drop(e) {
    e.preventDefault();
    const data =  e.dataTransfer.getData("text");
    this.appendChild(document.getElementById(data));
}

function dragDrop () {
    const listItem = document.querySelectorAll(".cards div");
    listItem.forEach( item => {
        item.addEventListener("dragstart", drag)
    });

    const table = document.getElementsByClassName("cards")
    for (let i = 0; i < table.length; i++) {
        table[i].addEventListener("dragover", allDrop);
        table[i].addEventListener("drop", drop);
    }
}

container.addEventListener("click", (e) => {
    if (e.target.className !== "sort-btn") return;
    const sortBtn = e.target.closest(".sort-btn");
    const cards = sortBtn.nextSibling;
    console.log(cards);
    const cardsToSort = [].slice.call(cards.children);
    console.log(cardsToSort);
    const sortedCards = cardsToSort.sort(function(a, b ) {
        let textA = a.textContent.toLowerCase();
        let textB = b.textContent.toLowerCase();
        if (textA < textB)
            return -1;
        if (textA > textB)
            return 1;
        return 0
    });
    cards.innerHTML = sortedCards.map(el => el.outerHTML).join("");
    dragDrop ();
});

const addColumnBtn = document.getElementById("new-column-btn");
addColumnBtn.addEventListener("click", () => {
    Column.createColumn();
});
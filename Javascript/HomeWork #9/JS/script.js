// Каждой закладке tab с атрибутом data-toggle-id соответсвует своя вкладка с текстом с #id

let  tabs = document.querySelectorAll(".tabs-title"); // Создаю переменную с массивом закладок li
let activeTab;                                                // Создаю перменную, которая будет ловить активную закладку li

for (let tab of tabs) {                                       // Через цикл перебираю все закладки и вешаю на них обработчик события "клик"
    tab.addEventListener("click", event => {
        let target = event.target;                            // Ловлю элемент-закладку, на которой произошло событие
        activeText(target);                                   // На кликнутой закладке выполняю функцию activeText()
    });
}

function activeText(tabX) {
    if (activeTab) {                                  // если активная закладка уже существует
        activeTab.classList.remove("active");  // убираем ей CSS класс "active"
        let id = activeTab.dataset.toggleId;          // сожержимое атрибута data-toggle-id пишем в перменную id
        let elem = document.getElementById(id);       // ищем элемент по #id такому же как в переменной id
        elem.hidden = true;                           // прячем элемент
    }
    activeTab = tabX;                                  // переменной присваиваю значение нажатой закладки
    activeTab.classList.add("active");                // закладке добавляю CSS класс "active"
    let id = activeTab.dataset.toggleId;              // сожержимое атрибута data-toggle-id пишем в перменную id
    let elem = document.getElementById(id);           // ищем элемент по #id такому же как в переменной id
    elem.hidden = false;                              // показываем элемент
}

//=============================================
// Вариант с несколькими нажатыми табами

// for (let tab of tabs) {
//     tab.addEventListener("click", event => {
//
//         tab.classList.toggle("active");
//         let id = event.target.dataset.toggleId;
//         let elem = document.getElementById(id);
//
//         elem.hidden = !elem.hidden;
//     })
// };
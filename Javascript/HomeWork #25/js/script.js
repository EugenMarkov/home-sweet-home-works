const allSlides = document.querySelectorAll(".slider__item");    // Создаю переменную для всех li со слайдами
const slideToShow = document.querySelector(".slider__items");    // Создаю переменную для ul, которую буду двигать через transform: translateX
const previousBtn = document.getElementById("previous");        // Создаю переменную для кнопки PREVIOUS
const nextBtn = document.getElementById("next");                // Создаю переменную для кнопки NEXT

let slideIndex = 0;                                 // Создаю переменную, которая будет ловить индекс активного сдайда (изначально 0)
previousBtn.addEventListener("click", () => {                // Вешаю обработчик клика на кнопку PREVIOUS
    slideToggle(slideIndex - 1);                                    // Работает функция переключения слайдов назад
});

nextBtn.addEventListener("click", () => {                    // Вешаю обработчик клика на кнопку NEXT
    slideToggle(slideIndex + 1);                                    // Работает функция переключения слайдов вперед
});

function slideToggle(index) {                                              // Сама функция переключения слайдов
    allSlides[slideIndex].classList.remove("active-slide");         // Удаляем класс "active-slide" слайду со старым индексом
    slideIndex = (index + allSlides.length) % allSlides.length;            // Получаем новый индекс (нажимая на кнопки)
    allSlides[slideIndex].classList.add("active-slide");                   // Добавляем класс "active-slide" слайду с новым индексом
    const activeSlide = document.querySelector(".active-slide");  // Создаем переменную для слайда с классом "active-slide"
    slideToShow.style.transform = "translateX(" + activeSlide.dataset.pos + ")";
}           // Двигаем список ul через transform: translateX, получая значение из data-pos конкретного слайда li.

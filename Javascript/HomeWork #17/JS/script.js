let userNumber = prompt("Enter your integer number","0");
let wrongValue = userNumber;

while (!Number.isInteger(+userNumber) || userNumber === null || userNumber === "" || userNumber.includes(" ")) {
    if (userNumber === null) {
        userNumber = prompt("Enter correct integer number","");
    } else {
        userNumber = prompt("Enter correct integer number", wrongValue)
    }
    wrongValue = userNumber;
}

function fibonacci(F0, F1, n) {
    n = +n;
    if (n === 0) {
        return F0;
    } else if (n === 1) {
        return F1;
    }
    else if ( n > 1) {
        return fibonacci (F0, F1, n - 1) + fibonacci (F0, F1, n - 2);
    }
    else  {
        return fibonacci (F0, F1, n + 2 ) - fibonacci (F0, F1, n + 1);
    }
}

alert(fibonacci(0, 1, userNumber));




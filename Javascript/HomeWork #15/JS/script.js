$(".header-anchor-menu-link").on("click", (event) => {                 //Вешаю обработчик события клик на jQuery коллекцию ссылок
    event.preventDefault();                                            //Отменяю стандартный обработчик события
    const $id  = $(event.target).attr('href');                         //В переменную $id получаю значение атрибута href кликнутой ссылки
    const $sectionTop = $($id).offset().top;                           //В переменную $sectionTop получаю значение координаты top секции с id равному значению в переменной $id
    $("body, html").animate({scrollTop: $sectionTop}, 1000);       //На боди и html  вешается анимация прокрутки до координаты top в переменной $sectionTop
});

$(document).scroll(() => {                                                 //На документ вешаю обработчик события скролл
    if ($(window).scrollTop() > $(window).innerHeight()) {                 //Если скролл прокрутки больше высоты окна, то
        if (!$(".up-btn").length) {                                        //Проверяю существование кнопки через длину jQuery коллекции
            const $upBtn = $('<button hidden class="up-btn">Up</button>'); //Создаю кнопку с классом "up-btn" и прячу ее через hidden
            $("script:first").before($upBtn);                              //Размещаю кнопку перед первым скриптом
            $upBtn.fadeIn(500);                                            //Плавно показываю кнопку

            $upBtn.click(() => {                                                   //Вешаю обработчик клика пл кнопке Up
                $("body, html").animate({scrollTop: 0}, 1000);   //На боди и html  вешается анимация прокрутки к верху документа
            });
        }
    } else {                                                               //Если скролл прокрутки не больше высоты окна
        $(".up-btn").fadeOut(500, () => {                                  //Кнопка Up плавно скрывается
            $(".up-btn").remove();                                         //И удяляется
        });
    }
});

$(".toggle-news-btn").on("click", (event) => {                              //Вешаю обрабочик клика по кнопке "Hot news"
    $(".news-content").slideToggle(1500);                                   //Плавно сворачиваю или разворачиваю блок с контентом
    $(".news-title").slideToggle();                                         //И заголовок
});
const oldObject = {"a" : 1, "b" : 2, "c": {"x" : 3, "y" : 4, "z" : {name : "gogi"}}, function: f(), alpha:[1 , 2, null, {betta: {gamma:[111, 137]}},[undefined],[3, 4, [5, 6]]]};
function f() {
return console.log("Function is working!")
}

function clone (object) {
    if (Array.isArray(object)) {
        let newArr = [];
        for (let i = object.length; i-- > 0;) {
            newArr[i] = clone(object[i]);
        }
        return newArr;
    }
    if (typeof object === "object" && object !== null) {
        let newObj = {};
        for (let item in object) {
            if (object.hasOwnProperty(item)) {
                newObj[item] = clone(object[item]);
            } else {
                this[item] = object[item]
            }
        }
        return newObj;
    }
    return object;
}
const newObject = clone(oldObject);
const alternativeNewObject = JSON.parse(JSON.stringify(oldObject));

newObject.alpha[3]["betta"]["gamma"] = [777];
alternativeNewObject.alpha[5][0] = {name: "surprise"};

console.log(oldObject);
console.log(newObject);
console.log(alternativeNewObject);

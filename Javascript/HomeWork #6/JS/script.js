function filterBy(array, typeOfData) {
    if (typeOfData === "null") {
        return array.filter(elem => elem !== null )
    } else if (typeOfData === "object") {
        return array.filter(elem => typeof elem !== typeOfData || elem === null)
    } else {
        return array.filter(elem => typeof elem !== typeOfData)
    }
}

let arrTest =[null, false,{}, "ffff",[1, 3, 4], "null", NaN, Symbol("id"),"gogi",null, 777, undefined, "undefined", "stringhhh", 78, 1, 4, true, 555, 5, 7,];

console.log(filterBy(arrTest, "number"));
console.log(filterBy(arrTest, "null"));
console.log(filterBy(arrTest, "object"));
console.log(filterBy(arrTest, "boolean"));
console.log(filterBy(arrTest, "string"));
console.log(filterBy(arrTest, "undefined"));
console.log(filterBy(arrTest, "symbol"));
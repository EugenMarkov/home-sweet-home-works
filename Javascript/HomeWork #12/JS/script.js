const images = document.querySelectorAll(".image-to-show");   // Переменная для массива с картинками
const wrapper = document.querySelector(".images-wrapper");    // Переменная для блока обертки картинок

function slideImg(pictures, container) {                               // Создаю функцию-слайдер скрываюшую и показывающую картинки
    pictures.forEach((pictureN) => {                                   // Каждой картинке через forEach добавляю класс CSS
        pictureN.classList.add("image-to-hide");                       // "image-to-hide" с opacity 0 и position: absolute (прячем картинки и складываем их одна на другую)
    });
    container.classList.add("images-wrapper-slider");                  // Блоку обертке даем класс CSS с высотой, шириной и красивой рамкой

    let currentImage  = 0;                                             // Создаем переменную-индекс для текущей картинки
    let imagesInterval = setInterval(showImages, 10000);       // Переменная запуска setInterval с функцией showImages и задержкой 10 секунд

    function showImages () {                                           // Сама магическая функция показа картинок
    images[currentImage].classList.remove("current-image");     // Удаляем картинке с индексом из переменной currentImage класс CSS "current-image" (начиная с 0)
    currentImage = (currentImage+1) % images.length;                   // Присваивание переменной currentImage значения остатка деления старого значения +1 на длину массива, что создает цикл (это настоящая магия!!!)
    images[currentImage].classList.add("current-image");               // Картинке со следующим индексом добавляем класс "current-image" с opacity 1 и т.д.
    }
                                                                       // Создаю кнопки управления
    const buttons = document.createElement("div");            // Создаю блок для кнопок и добавляю ему стиль
    buttons.classList.add("btns");
    const stopBtn = document.createElement("button");         // Создаю кнопоку паузы и добавляю ей стиль и текст
    stopBtn.classList.add("btn");
    stopBtn.innerText = "Прекратить";
    const runBtn = document.createElement("button");          // Создаю кнопоку возобновления и добавляю ей стиль и текст
    runBtn.classList.add("btn");
    runBtn.innerText = "Возобновить показ";
    buttons.append(stopBtn, runBtn);                                   // Размещаю кнопки внутри блока
    wrapper.after(buttons);                                            // Блок размещаю после обертки для картинок

    stopBtn.addEventListener("click", () => {             // Вещаю оюработчик клика по паузе
    clearInterval(imagesInterval);                                     // Который удаляет setInterval по таймеруId в переменной imagesInterval
    });
    runBtn.addEventListener("click", () => {              // Вещаю оюработчик клика по кнопке возобновления
    imagesInterval = setInterval(showImages, 10000);            // Снова запускается setInterval с функцией showImages
    });
}

slideImg(images,wrapper);


//============================================= Первый вариант ==================================================

// const images = document.querySelectorAll(".image-to-show");
// const wrapper = document.querySelector(".images-wrapper")
// console.log(images);

// let currentImage  = 0;
// let imagesInterval = setInterval(showImages, 2000);

//
// function showImages () {
//     images[currentImage].classList.remove("current-image");
//     currentImage = (currentImage+1) % images.length;
//     images[currentImage].classList.add("current-image");
// }
//
// const stopBtn = document.querySelector(".stop");
// const runBtn = document.querySelector(".run");
//
// stopBtn.addEventListener("click", () => {
//     clearInterval(imagesInterval);
// });
//
// runBtn.addEventListener("click", () => {
//     imagesInterval = setInterval(showImages, 2000);
// });


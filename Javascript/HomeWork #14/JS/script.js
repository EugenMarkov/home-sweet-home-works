
// Каждой закладке tab с атрибутом data-toggle-id соответсвует своя вкладка с текстом с #id
// Так как в jQuery нет метода dataset использую .attr() для получения значения атрибутов

let $activeTab = $(".active");                        // Создаю перменную, которая будет ловить активную закладку li

$(".tabs-title").on("click", event => {               // Через jQuery коллекцию сразу на все закладки вешаю  обработчик события "клик"
        const $target = event.target;                 // Ловлю элемент-закладку, на которой произошло событие
        activeText($target);                          // На кликнутой закладке выполняю функцию activeText()
    });

function activeText(tabX) {
    if ($activeTab) {                                  // если активная закладка уже существует
        findAndShow("data-toggle-id");        // убираем ей CSS класс "active" и прячем элемент  с помощью функции  findAndShow()
    }
    $activeTab = tabX;                                 // переменной присваиваю значение нажатой закладки
    findAndShow("data-toggle-id");            // закладке добавляю CSS класс "active" и показываю элемент с помощью функции  findAndShow()
}

function findAndShow(dataClass) {                      // функция, которая тогглит класс "active" и тогглит элементы с текстом
    $($activeTab).toggleClass("active");
    const $id = $($activeTab).attr(dataClass);         // сожержимое атрибута в параметре функции пишем в переменную $id
    $(`li[id = ${$id}]`).toggle();                     // находим элемент со значением атрибута id равным значению в переменной $id и тогглим его
}

const student = {};
student.property = "name"; // без этой строчки можно обойтись, но в задании указано создать поле "name"
student.property = "last name"; // а тут создаю поле "last name"

student.name = prompt("Enter name","Vasja");
student["last name"] = prompt("Enter last name","Petrov");
student.table = []; // Создаю пустой массив табель для баллов

let subject; // переменная для названий предметов
while (subject !== null) {
    subject = prompt("Enter name of subject?","Programming");
   if (subject !== null) {
       let grade = prompt("Enter your grade","5");
       student.table.push(+grade);// заполняю табель баллами
   }
}

let counter1 = 0; // переменная для подсчета предметов с баллом < 4
let counter2 = 0; // переменная для подсчета ощего числа предметов
let sumGrade = 0; // переменная для подсчета общей суммы баллов по всем предметов
// В одном цикле сразу считается общая сумма баллов по предметам и количество предметов с баллом < 4
for (let key in student.table) {
    sumGrade += student.table[key];
    counter2 ++;
    if (student.table[key] < 4) {
        counter1++;
    }
}

if (counter1 === 0) {
    console.log("Студент переведен на следующий курс")
}

const avrgGrade = sumGrade/student.table.length; //Вариант среднего балла через длину массива (тогда не нужна переменная counter2)
const avrgGrade2 = sumGrade/counter2; // Вариант подсчета среднего балла через переменную counter2
// console.log(avrgGrade, avrgGrade2); // Средний балл из первого и второго варианта
if (avrgGrade > 7) {
    console.log("Студенту назначена стипендия!");
}
// или так
// if (avrgGrade2 > 7) {
//     console.log("Студенту назначена стипендия!");
// }

console.log(student);
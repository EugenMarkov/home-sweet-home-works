const themeBtn = document.getElementById("theme");           //Создаю переменные для элементов, которым надо менять стили
const links = document.querySelectorAll(".nav-bar-link");
const startBtn = document.getElementById("main-start");
const mainWrapper = document.querySelector(".main-gray-bg");

function changeTheme () {                                      //Создаю функцию смены темы, которая добавляет или удаляет CSS классы
    themeBtn.classList.toggle("alternative-btn");
    links.forEach((link) => {
        link.classList.toggle("alternative-theme");
    });
    startBtn.classList.toggle("alternative-btn");
    mainWrapper.classList.toggle("alternative-bg");
}

themeBtn.addEventListener("click", () => {         //Вешаю обработчик клика на кнопку
    changeTheme();                                              //Функция работает как переключатель
    if (localStorage.getItem("theme") === null) {          //Если в локальном зхранилище нет темы, создаем ключ "theme" со значением "indigo"
        localStorage.setItem("theme","indigo");
    } else {
        localStorage.removeItem("theme")                   //Если в локальном хранилище есть тема, удаляем ключ "theme"
    }
});

window.addEventListener("load", () => {           //Вешаю обработчик загрузки окна
    if (localStorage.getItem("theme") !== null) {          //Если в локальном хранилище есть тема (ключ "theme")
        changeTheme();                                          //Функция changeTheme() меняет стили
    }
});


let numberOne = prompt("Enter first number","0");
let defaultNumberOne = numberOne;
while (isNaN(numberOne) || numberOne === "" || numberOne === null)  {
    if (numberOne === null) {
        numberOne = prompt("Enter first number again","");
    }else{
        numberOne = prompt("Enter first number again","" +defaultNumberOne);
    }
    defaultNumberOne = numberOne;
};

let numberTwo = prompt("Enter second number","0");
let defaultNumberTwo = numberTwo;
while (isNaN(numberTwo) || numberTwo === "" || numberTwo === null) {
    if (numberTwo === null) {
        numberTwo = prompt("Enter second number again","");
    }else{
        numberTwo = prompt("Enter second number again","" +defaultNumberTwo);
    }
    defaultNumberTwo = numberTwo;
};


let calculation = prompt("Enter operation","*");
while (calculation !== "+" && calculation !== "-" && calculation !== "*" && calculation !== "/") {
    calculation = prompt("Enter operation again", "*");
};


function calculator(numberFirst, numberSecond, operation) {
    let result;
    if (operation === "+") {
        result = +numberFirst + +numberSecond;
    }
    else if (operation === "-") {
        result = numberFirst - numberSecond;
    }
    else if (operation === "/") {
        result = numberFirst/numberSecond;
    }
    else if (operation === "*")  {
        result = numberFirst * numberSecond;
    } else {
        console.log("Error")
    }
    return result;
}

console.log(calculator(numberOne, numberTwo, calculation));


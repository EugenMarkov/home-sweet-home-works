const label = document.createElement("label");  // Создаю  <label> с меткой "Price, $ "
label.innerText = "Price, $ ";
document.querySelector("script").before(label); // Помещаю label перед script

const input = document.createElement("input");  // Создаю числовой <input> со значением "0"
input.type = "number";
input.value = "0";
label.append(input);                                     // Помещаю input перед внутри label

const span = document.createElement("span");    // Создаю  <span>
span.style.display = "block";                            // display = "block" для span, чтобы был над input, а не в одну строку
span.style.marginBottom ="10px";                         // добавил отступ вниз 10px, чтобы не сливался с  input

const btn = document.createElement("button");   // Создаю  кнопку "х"
btn.innerText = "x";

const error = document.createElement("p");      // Создаю <p> с текстом и добавляю стиль для красоты
error.innerText = "Please enter correct price.";
error.style.fontWeight =  "bold";
error.style.color = "red";

input.addEventListener("focus", (event => {        // Создаю обработчик для события фокус для поля ввода и добавляю стили
    input.style.borderColor = "green";
    input.style.backgroundColor = "";
}));
input.addEventListener("blur", (event => {     // Создаю обработчик для события расфокусировки для поля ввода и добавляю стили
    if (input.value < 0) {                           // Условие для цены меньше 0
        input.style.borderColor = "red";
        label.after(error);                          // Размещаю текст об ошибке под label
        span.remove();                               // Удаляю span на случай (если сначала ввели цену >= 0 и span появился над полем
    } else {                                         // Условие для цены больше или равной 0
        input.style.borderColor = "";
        input.style.backgroundColor = "green";
        label.before(span);                                 // Размещаю span над label
        span.innerText = `Текущая цена: ${input.value}   `; // Добавляю текст в span со значением из поля input (обхожусь без переменной)
        span.append(btn);                                   // Размещаю кнопку btn внутри span
        error.remove();                                     // Удаляю сообщение о некорректной цене (если оно было)
    }
}));
btn.addEventListener("click", (event => {      // Создаю обработчик для клика по кнопке btn
    span.remove();                                   // Клик удаляет span и кнопку внутри себя
    input.value = "0";                               // Обнуляем значение поля ввода input
    input.style.backgroundColor = "";                // И удаляем полю ввода цвет фона
}));
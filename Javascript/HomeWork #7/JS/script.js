let arr1 = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv','Berdichev'];
let arr2 = ['1', '2', '3', 'sea', 'user', 23, null, NaN, undefined, true];

function listedArray(array) {
    const ul = document.createElement("ul");
    ul.classList.add("list");

    const addLiArray = array.map(elem => {
        const li = document.createElement("li");
        li.innerText = elem;
        return li;
    });

    addLiArray.forEach((elem) => ul.appendChild(elem));
    return ul;
}

document.querySelector("script").before(listedArray(arr1));
document.querySelector("script").before(listedArray(arr2));







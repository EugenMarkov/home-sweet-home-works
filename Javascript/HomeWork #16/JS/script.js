let userNumber = prompt("Enter your positive natural number or 0","0");
let wrongValue = userNumber;

while (!Number.isInteger(+userNumber) || userNumber < 0 || userNumber === null || userNumber === "" || userNumber.includes(" ")) {
    if (userNumber === null) {
        userNumber = prompt("Enter correct positive natural number or 0","");
    } else {
        userNumber = prompt("Enter correct positive natural number or 0", wrongValue)
    }
    wrongValue = userNumber;
}

function factorial (number) {
    number = +number;
    if (number === 0) {
        return 1;
    } else {
        return number*factorial(number - 1)
    }
}

alert(factorial(userNumber));



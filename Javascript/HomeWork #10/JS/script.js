const topPass = document.querySelector("#topPass");              // Создаю переменную для верхнего поля ввода пароля
const eyeTop = document.querySelector("#eyeTop");                // Создаю переменную для верхней иконки с глазом

eyeTop.addEventListener("mousedown", event => {             // Вешаю обработчик события нажатия на верхнюю иконку
    topPass.type = "text";                                               // Меняю полю ввода тип на текстовый
    eyeTop.classList.replace("fa-eye","fa-eye-slash") // Иконку меняю на зачеркнутый глаз
});
eyeTop.addEventListener("mouseup", event => {               // Вешаю обработчик события отжатия на верхнюю икноку
    topPass.type = "password";                                           // Меняю полю ввода тип на "пароль"
    eyeTop.classList.replace("fa-eye-slash","fa-eye") // Иконку меняю на обычный глаз
});

const bottomPass = document.querySelector("#bottomPass");        // Создаю переменную для нижнего поля ввода пароля
const eyeBottom = document.querySelector("#eyeBottom");          // Создаю переменную для нижней иконки с глазом

eyeBottom.addEventListener("mousedown", event => {          // Вешаю обработчик события нажатия на нижнюю иконку
    bottomPass.type = "text";                                             // Меняю полю ввода тип на текстовый
    eyeBottom.classList.replace("fa-eye","fa-eye-slash") // Иконку меняю на зачеркнутый глаз
});
eyeBottom.addEventListener("mouseup", event => {            // Вешаю обработчик события отжатия на нижнюю икноку
    bottomPass.type = "password";                                         // Меняю полю ввода тип на "пароль"
    eyeBottom.classList.replace("fa-eye-slash","fa-eye")  // Иконку меняю на обычный глаз
});

const btn = document.querySelector(".btn");                      // Создаю переменную для кнопки
const bottomLabel = document.querySelector(".bottom-label");     // Создаю переменную для нижней метки label
const error = document.createElement("p");                       // Создаю элемент "р" и присваиваю в переменную error
error.innerText = "Нужно ввести одинаковые значения";                     // Присваиваю error текст
error.style.color = "red";                                                // Задаю error стиль: цвет текста

const form = document.querySelector(".password-form");           // Создаю переменную для формы
form.addEventListener("submit", event => {                  // Вешаю обработчик на отправку формы
    event.preventDefault();                                               // Отменяю событие (чтобы страница не перегружалась)
});

btn.addEventListener("click", event => {                   // Обработчик события клик по кнопке btn
    if (topPass.value === bottomPass.value) {                            // если сожержимое полей ввода паролей совпадает
        error.remove();                                                  // удаляю элемент error
        setTimeout(() => alert("You are welcome"), 100); // с задержкой вывожу alert, без задержки error не успевает удалитсья
    } else {                                                             // если пароли не совпадают, то
        bottomLabel.after(error);                                        // размещаю error под меткой bottomLabel
    }
});

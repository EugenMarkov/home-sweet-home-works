const allOurBtns = document.querySelectorAll(".btn");         // Создаю переменную c массивом всех кнопок

function light(buttons) {                                              //Создаю функцию подсветки кнопок для потенциального массива кнопок
    let highLightedBtn;                                                // Создаю переменную, которая будет ловить подсвеченную кнопку
    document.addEventListener("keydown", (event) => {     // Вешаю обработчик нажатия кнопок на весь документ
        for (let btn of buttons) {                                     // Через цикл проверяю каждую кнопку
            if ( btn.innerText === event.code.slice(-1) || btn.innerText === event.code) { // Проверка совпадения сожержимого кнопок
           // c последней буквой кода нажатой клавиши   или со всем кодом (для "Enter")     (работает для разных языков)
                highLight(btn)                                         // если проверка удачная, работает функция подсветки
            }
        }
    });

    function highLight(newBtn) {                                        // Собственно сама функция переключения класса с фоном
        if (highLightedBtn) {                                           // Если подсвеченная кнопка уже есть
            highLightedBtn.classList.remove("highlight")         // Удаляем ей класс с фоном
        }
        highLightedBtn = newBtn;                                         // Переменная ловит новую кнопку
        highLightedBtn.classList.add("highlight");                       // Добавляем новой кнопке класс с фоном
    }
}

light(allOurBtns);



// ================================================================== Вариант без единой функции
// const allOurBtns = document.querySelectorAll(".btn");         // Создаю переменную c массивом всех кнопок
// let highLightedBtn;                                           // Создаю переменную, которая будет ловить кнопку

// document.addEventListener("keydown", (event) => {              // Вешаю обработчик на документ
//     for (let btn of allOurBtns) {                              // Перебираю массив кнопок
//         // if (btn.innerText === event.key || btn.innerText === event.key.toUpperCase()) { Проверка содержимого кнопок на совпадение с нажатием клавиш в разных регистрах (работает для English)
//
//             if ( btn.innerText === event.code.slice(-1) || btn.innerText === event.code) { // Проверка совпадения сожержимого кнопок и последней буквы кода клавиши клавиатуры и совпадения для "Enter" и т.п. (работает для разных языков)
//             console.log(event.code);
//             console.log(event.key);
//             highLight(btn)                                    // если проверка удачная, работает функция подсветки
//          }
//     }
// });
//
// function highLight(newBtn) {                                        // Собственно сама функция переключения класса с фоном
//     if (highLightedBtn) {                                           // Если подсвеченная кнопка уже есть
//         highLightedBtn.classList.remove("highlight")                // Удаляем ей класс с фоном
//     }
//     highLightedBtn = newBtn;                                         // Переменная ловит новую кнопку
//     highLightedBtn.classList.add("highlight");                       // Добавляем новой кнопке класс с фоном
// }
//

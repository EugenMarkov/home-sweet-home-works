import React, { Component } from 'react';
import Card from "../Card/Card";
import Modal from "../Modal/Modal";

class Goods extends Component {
    constructor(props) {
        super(props);
        this.state = {
            goods: [],
            // cart: []
        };
    }

    getGoodsFromBD() {
        fetch('./goods.json')
            .then(response => response.json())
            .then(data => this.setState({...data}));
    }

    render() {
        this.getGoodsFromBD();
        return (
            <div>
                <div id="modal-container">
                    <Modal id={"modal-add-to-cart"} header={"Do you want to add to cart new good?"}
                           text={"Are you sure you want to add to cart"}
                           actions={[{text: "OK", onClick: this.clickModalAddToCartBtn.bind(this)},
                               {text: "CANCEL",
                                   onClick: ()=> document.getElementById("modal-add-to-cart").classList.add("modal-hide")}]}
                    />
                </div>
                <p className="goods-list-title">OUR BEST TOURS</p>
                <div className="goods-list" id="good-list">
                    {this.state.goods.map(item => {
                        const idx = this.state.goods.indexOf(item);
                        const favouriteGood = localStorage.getItem(`Favourite${idx}`);
                        // console.log(JSON.parse(favouriteGood));
                        const favouriteGoodToBoolean = Boolean(favouriteGood);
                        const addedToCartGood = localStorage.getItem(`AddToCart${idx}`);
                        const addedToCartGoodToBoolean = Boolean(addedToCartGood);
                        // console.log(favouriteGoodToBoolean);
                        // console.log(addedToCartGoodToBoolean);
                        return (
                                <Card id={idx} key={item.url} title={item.title} price={item.price} url={item.url}
                                      articulus={item.articulus} isFavourite={favouriteGoodToBoolean}
                                      addToCart={addedToCartGoodToBoolean}
                                />
                        )
                    })}
                </div>
            </div>
        )
    }

    clickModalAddToCartBtn() {
       const el = document.getElementById("modal-good-title");
       const id = el.getAttribute("data-id");
       console.log(id);
        if(localStorage.getItem(`AddToCart${id}`)){
            console.log("Good was added before");
        } else {
            localStorage.setItem(`AddToCart${id}`, JSON.stringify("Added to Cart") );
        }
        console.log(localStorage.getItem(`AddToCart${id}`));

       document.getElementById("modal-add-to-cart").classList.add("modal-hide");
    }

}

export default Goods;

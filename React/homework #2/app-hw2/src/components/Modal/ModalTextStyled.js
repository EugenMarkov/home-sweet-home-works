import styled from 'styled-components';

const ModalTextStyled = styled.p`
  height: 70px;
  font-size: 15px;line-height: 2;
  text-align: center;
`;

export default ModalTextStyled;

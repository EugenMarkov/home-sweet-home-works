import styled from 'styled-components';

const ModalStyled = styled.div`
  position: fixed;
  left: 50%;
  top: 15%;
  transform: translateX(-50%);
  width: 516px;
  height: 252px;
  padding: 20px;
  color: white;
  font-family: sans-serif;
  font-size: 14px;
  border-radius: 5px;
  background-color: #e74c3c;
  z-index: 3;
  opacity: 1;
`;
export default ModalStyled;

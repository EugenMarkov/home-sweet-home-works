import styled from 'styled-components';

const ModalTitleStyled = styled.div`
  display: flex;
  border-right: 5px;
  align-items: center;
  height: 68px;
  font-size: 22px;
`;

export default ModalTitleStyled;

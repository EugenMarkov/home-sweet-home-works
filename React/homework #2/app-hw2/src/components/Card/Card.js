import React, { Component } from "react";
import Button from '../Button/Button.js';

class Card extends Component {
    constructor(props){
        super(props);
        this.id = props.id;
        this.title = props.title;
        this.price = props.price;
        this.url = props.url;
        this.articulus = props.articulus;
        this.clickFavouriteBtn = this.clickFavouriteBtn.bind(this);
        this.state = {
            isFavourite: props.isFavourite,
            addToCart: props.addToCart
        }

    }

    render() {
        const favouriteBtn = this.state.isFavourite ?
            <Button className="card-add-to-favourite-btn" text={<i className="fas fa-star"> </i>} onClick={this.clickFavouriteBtn.bind(this)}/>
          : <Button text={<i className="fas fa-star"> </i>} onClick={this.clickFavouriteBtn.bind(this)}/>;

        return(
            <div id={this.id} className="card">
                <img className="card-img" src={this.props.url} alt=""/>
                <p className="card-title">{this.title}</p>
                <p className="card-info">Lorem ipsum dolor sit amet, con
                    adipiscing elit, sed diam nonu. </p>
                <div className="card-details" >
                   <span>price: ${this.price}</span>
                   <div className="card-buttons">
                       {favouriteBtn}
                       <Button text={"ADD TO CART"} onClick={this.clickAddToCartBtn.bind(this)}/>
                   </div>
               </div>
            </div>
        )
    }

    clickFavouriteBtn(e){
        this.setState({isFavourite: !this.state.isFavourite});
        if(localStorage.getItem(`Favourite${this.id}`)){
            localStorage.removeItem(`Favourite${this.id}`)
        } else {
            localStorage.setItem(`Favourite${this.id}`, JSON.stringify("Favourite") );
        }
        console.log(localStorage.getItem(`Favourite${this.id}`));
        const target = e.target.closest("BUTTON");
        if(target.classList.contains("card-add-to-favourite-btn")){
            target.classList.remove("card-add-to-favourite-btn")
        } else {
        target.classList.add("card-add-to-favourite-btn")
        }
    }

    clickAddToCartBtn(){
        document.getElementById("modal-good-title").innerText = `${this.title}?`;
        document.getElementById("modal-good-title").dataset.id = `${this.id}`;
        document.getElementById("modal-add-to-cart").classList.remove("modal-hide");
    }

}

export default Card;

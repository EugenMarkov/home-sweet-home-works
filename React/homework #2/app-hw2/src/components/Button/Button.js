import React, { Component} from 'react';
import PropTypes from 'prop-types';
import ButtonStyled from './ButtonStyled';

class Button extends Component {
    constructor(props){
        super(props);
        this.backgroundColor = props.backgroundColor;
        this.text = props.text;
        this.onClick = props.onClick;
        this.className = props.className;
    }

    render() {
        return(
                <ButtonStyled className={this.className}
                    style={
                        {backgroundColor: this.backgroundColor}}
                        onClick={this.onClick}
                >{this.text}</ButtonStyled>
            )
    }
}

Button.defaultProps = {
    backgroundColor: "#1e1e20",
    className: ""
};

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text:  PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    onClick: PropTypes.func,
};

export default Button;

import styled from 'styled-components';

const ButtonStyled = styled.button`
margin: 5px;
height: 26px;
padding: 5px 10px;
border-radius: 5px;
border: none;
outline: none;
color: white;
cursor: pointer;
`;

export default ButtonStyled;

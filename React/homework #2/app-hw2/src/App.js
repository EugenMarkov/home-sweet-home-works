import React from 'react';
import Goods from "./components/Goods/Goods";


function App() {
  return (
    <div className="App">
        <div id="cards">
            <Goods/>
        </div>
    </div>
  );
}

export default App;

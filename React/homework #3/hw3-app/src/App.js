import React from 'react';
import Nav from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import Router from "./RouterNavbar.js";


function App() {
  return (
    <div>
        <Nav/>
        <Router/>
        <Footer/>
    </div>
  );
}

export default App;

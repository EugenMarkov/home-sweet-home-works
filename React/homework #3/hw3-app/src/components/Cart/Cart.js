import React, { useState } from 'react';
import CardAtCart from "../CardAtCart/CardAtCart";
import Modal from "../Modal/Modal";

function Cart() {
    const [ goodsAtCart, setGoodsAtCart] = useState([]);
    getGoodsAtCart();

    function getGoodsAtCart() {
     fetch('./goods.json')
    .then(response => response.json())
    .then(data => setGoodsAtCart(data));
    }

    return (
        <div>
            <Modal id={"modal-remove-good-from-cart"} header={"Do you want to remove good from cart?"}
                   text={"Are you sure you want to remove good from cart"}
                   actions={[{text: "OK", onClick: removeFromCartModalBtn},
                       {text: "CANCEL",
                           onClick: ()=> document.getElementById("modal-remove-good-from-cart").classList.add("modal-hide")}]}
            />
            <p className="goods-list-title">TOURS AT CART</p>
            <div className="goods-list" id="goods-at-cart-list">
                {goodsAtCart.map(item => {
                    const idx = goodsAtCart.indexOf(item);
                    const addedToCartGood = localStorage.getItem(`AddedToCart${idx}`);
                    const addedToCartGoodToBoolean = Boolean(addedToCartGood);
                    if(addedToCartGoodToBoolean){
                        return (
                            <CardAtCart id={idx} key={item.url} title={item.title} price={item.price} url={item.url}
                                  articulus={item.articulus}
                                  addToCart={addedToCartGoodToBoolean} modalId="modal-remove-good-from-cart"
                            />
                        )
                    }
                })}
            </div>
        </div>
    );

    function removeFromCartModalBtn() {
        const el = document.getElementById("modal-good-title");
        const id = el.getAttribute("data-id");
        if(localStorage.getItem(`AddedToCart${id}`)){
            localStorage.removeItem(`AddedToCart${id}`);
        }
        document.getElementById("modal-remove-good-from-cart").classList.add("modal-hide");
    }
}

export default Cart;

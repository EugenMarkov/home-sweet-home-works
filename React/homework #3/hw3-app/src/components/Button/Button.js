import React from 'react';
import PropTypes from 'prop-types';
import ButtonStyled from './ButtonStyled';

function Button(props) {
    const { backgroundColor, text, onClick, className } = props;

    return(
        <ButtonStyled className={className} style={{backgroundColor: backgroundColor}} onClick={onClick}>{text}</ButtonStyled>
    )
}

Button.defaultProps = {
    backgroundColor: "#1e1e20",
    className: ""
};

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text:  PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    onClick: PropTypes.func,
};

export default Button;

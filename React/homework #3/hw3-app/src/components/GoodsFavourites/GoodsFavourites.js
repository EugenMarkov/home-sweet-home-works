import React, { useState } from 'react';
import Card from "../Card/Card";
import Modal from "../Modal/Modal";

function GoodsFavourites() {
    const [goodsFavourites, setGoodsFavourites] = useState([]);
    getGoodsFavourites();

    function getGoodsFavourites() {
        fetch('./goods.json')
            .then(response => response.json())
            .then(data => setGoodsFavourites(data));
    }

    return(
        <div>
            <Modal id={"modal-add-to-cart-from-favGoods"} header={"Do you want to add to cart new good?"}
                   text={"Are you sure you want to add to cart"}
                   actions={[{text: "OK", onClick: AddToCartFromFavModalBtn},
                       {text: "CANCEL",
                           onClick: ()=> document.getElementById("modal-add-to-cart-from-favGoods").classList.add("modal-hide")}]}
            />

            <p className="goods-list-title">YOUR FAVOURITES TOURS</p>
            <div className="goods-list" id="favourites-goods-list">
                {goodsFavourites.map(item => {
                    const idx = goodsFavourites.indexOf(item);
                    const favouriteGood = localStorage.getItem(`Favourite${idx}`);
                    const favouriteGoodToBoolean = Boolean(favouriteGood);
                    const addedToCartGood = localStorage.getItem(`AddToCart${idx}`);
                    const addedToCartGoodToBoolean = Boolean(addedToCartGood);
                    if(favouriteGoodToBoolean) {
                        return (
                            <Card id={idx} key={item.url} title={item.title} price={item.price} url={item.url}
                                  articulus={item.articulus} isFavourite={favouriteGoodToBoolean}
                                  addToCart={addedToCartGoodToBoolean} modalId="modal-add-to-cart-from-favGoods"
                            />
                        )}
                  })}

            </div>

        </div>
    );

    function AddToCartFromFavModalBtn() {
        const el = document.getElementById("modal-good-title");
        const id = el.getAttribute("data-id");
        if(localStorage.getItem(`AddedToCart${id}`)){
            document.getElementById("modal-add-to-cart-from-favGoods").classList.add("modal-hide");
        } else {
            localStorage.setItem(`AddedToCart${id}`, JSON.stringify("Added to Cart") );
        }

        document.getElementById("modal-add-to-cart-from-favGoods").classList.add("modal-hide");
    };
}

export default GoodsFavourites;

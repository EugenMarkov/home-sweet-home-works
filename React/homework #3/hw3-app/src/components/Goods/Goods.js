import React, { useState } from 'react';
import Card from "../Card/Card";
import Modal from "../Modal/Modal";

function Goods () {
    const [goods, setGoods] = useState([]);
    getGoodsFromBD();
    localStorage.setItem("Goods list", JSON.stringify(goods));
    function getGoodsFromBD() {
        fetch('./goods.json')
            .then(response => response.json())
            .then(data => setGoods(data))
    }

        return (
            <div>
                <Modal id={"modal-add-to-cart-from-goods"} header={"Do you want to add to cart new good?"}
                       text={"Are you sure you want to add to cart"}
                       actions={[{text: "OK", onClick: AddToCartModalBtn},
                               {text: "CANCEL",
                                   onClick: ()=> document.getElementById("modal-add-to-cart-from-goods").classList.add("modal-hide")}]}
                />
                <p className="goods-list-title">OUR BEST TOURS</p>
                <div className="goods-list" id="goods-list">
                    {goods.map(item => {
                        const idx = goods.indexOf(item);
                        const favouriteGood = localStorage.getItem(`Favourite${idx}`);
                        const favouriteGoodToBoolean = Boolean(favouriteGood);
                        const addedToCartGood = localStorage.getItem(`AddToCart${idx}`);
                        const addedToCartGoodToBoolean = Boolean(addedToCartGood);
                        return (
                                <Card id={idx} key={item.url} title={item.title} price={item.price} url={item.url}
                                      articulus={item.articulus} isFavourite={favouriteGoodToBoolean}
                                      addToCart={addedToCartGoodToBoolean} modalId="modal-add-to-cart-from-goods"
                                />
                        )
                    })}
                </div>
            </div>
        );

   function AddToCartModalBtn() {
       const el = document.getElementById("modal-good-title");
       const id = el.getAttribute("data-id");
        if(localStorage.getItem(`AddedToCart${id}`)){
            document.getElementById("modal-add-to-cart-from-goods").classList.add("modal-hide");
        } else {
            localStorage.setItem(`AddedToCart${id}`, JSON.stringify("Added to Cart") );
        }

       document.getElementById("modal-add-to-cart-from-goods").classList.add("modal-hide");
    };

}

export default Goods;

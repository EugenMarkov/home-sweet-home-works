import React from 'react';
import Button from "../Button/Button.js";
import PropTypes from 'prop-types';
import ModalStyled from "./ModalStyled";
import ModalCloseBtnStyled from "./ModalCloseBtnStyled";
import ModalTitleStyled from "./ModalTitleStyled";
import ModalTextStyled from "./ModalTextStyled";
import ModalActionsBtnsStyled from "./ModalActionsBtnsStyled";
import ModalContentStyled from "./ModalContentStyled";

function Modal (props) {
    const { id, header, closeButton, text, actions } = props;

    const closeBtn = closeButton ? <ModalCloseBtnStyled onClick={()=> document.getElementById(id).classList.add("modal-hide")}>
    </ModalCloseBtnStyled> : null;

    return (
        <div className="modal-hide" id={id}>
            <div className="modal-overlap"
                 onClick={()=> document.getElementById(id).classList.add("modal-hide")}> </div>
            <ModalStyled>
                {closeBtn}
                <ModalTitleStyled>{header}</ModalTitleStyled>
                <ModalContentStyled>
                    <ModalTextStyled id="modal-text" className="modal-text">{text} <span id="modal-good-title"> </span>
                    </ModalTextStyled>
                    <img id="modal-img" src="" alt=""/>
                </ModalContentStyled>
                <ModalActionsBtnsStyled>
                    {actions.map((item)=> {
                        return (<Button key={item.text} text={item.text} onClick={item.onClick}/>)
                    })}
                </ModalActionsBtnsStyled>
            </ModalStyled>
        </div>
    )
}

Modal.defaultProps = {
    closeButton: true,
    actions: [{text: "OK", onClick: {}},
              {text: "CANCEL", onClick: ()=> document.getElementById("modal-add-to-cart").classList.add("modal-hide")}],
};

Modal.propTypes = {
    id:  PropTypes.string,
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.array,
    bgcBtn: PropTypes.string
};

export default Modal;

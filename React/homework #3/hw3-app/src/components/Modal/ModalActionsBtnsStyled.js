import styled from 'styled-components';

const ModalActionsBtnsStyled = styled.div`
display: flex;
align-items: center;
justify-content: center;
height: 30px;
margin-top: 10px;
margin-bottom: 10px;
`;

export default ModalActionsBtnsStyled;

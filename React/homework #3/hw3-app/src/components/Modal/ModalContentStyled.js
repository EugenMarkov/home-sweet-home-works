import styled from 'styled-components';

const ModalContentStyled = styled.div`
  height: 150px;
  text-align: center;
`;

export default ModalContentStyled;

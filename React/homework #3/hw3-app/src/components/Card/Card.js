import React, { useState } from "react";
import Button from '../Button/Button.js';

function Card (props){
    const { id, title, price, url, articulus, modalId } = props;
    const [isFavourite, setFavourite] = useState(props.isFavourite);

        const favouriteBtn = isFavourite ?
            <Button className="card-add-to-favourite-btn" text={<i className="fas fa-star"> </i>} onClick={clickFavouriteBtn}/>
          : <Button text={<i className="fas fa-star"> </i>} onClick={clickFavouriteBtn}/>;

        return(
            <div id={id} className="card">
                <img className="card-img" src={`${url}300/300`} alt=""/>
                <p className="card-title">{title}</p>
                <p className="card-code">code: {articulus}</p>
                <p className="card-info">Lorem ipsum dolor sit amet, con
                    adipiscing elit, sed diam nonu. </p>
                <div className="card-details" >
                    <p>price: <span className="card-price">${price}</span></p>
                   <div className="card-buttons">
                       {favouriteBtn}
                       <Button text={"ADD TO CART"} onClick={clickAddToCartBtn}/>
                   </div>
               </div>
            </div>
        );


   function clickFavouriteBtn(e){
       setFavourite(!isFavourite);
       if(localStorage.getItem(`Favourite${id}`)){
           localStorage.removeItem(`Favourite${id}`)
       } else {
           localStorage.setItem(`Favourite${id}`, JSON.stringify("Favourite") );
       }

        const target = e.target.closest("BUTTON");
        if(target.classList.contains("card-add-to-favourite-btn")){
            target.classList.remove("card-add-to-favourite-btn")
        } else {
        target.classList.add("card-add-to-favourite-btn")
        }
    }

    function clickAddToCartBtn(){
        document.getElementById("modal-good-title").innerText = `${title}?`;
        document.getElementById("modal-good-title").dataset.id = `${id}`;
        document.getElementById("modal-img").src = `${url}100/100`;
        document.getElementById(modalId).classList.remove("modal-hide");
    }
}

export default Card;

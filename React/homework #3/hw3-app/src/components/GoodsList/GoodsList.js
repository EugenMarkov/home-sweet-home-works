import React, { useState } from "react";

function GoodsList() {
    const [goods, setGoods] = useState([]);
    getGoodsFromBD();


    function getGoodsFromBD() {
        fetch('./goods.json')
            .then(response => response.json())
            .then(data => setGoods({...data}));
        console.log(goods);
    }

}

export default GoodsList;

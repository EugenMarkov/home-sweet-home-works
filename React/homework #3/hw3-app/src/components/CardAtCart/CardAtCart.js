import React, { useState } from "react";
import Button from '../Button/Button.js';

function CardAtCart (props){
    const { id, title, price, url, articulus, modalId } = props;

    return(
        <div id={id} className="card">
            <img className="card-img" src={`${url}300/300`} alt=""/>
            <p className="card-title">{title}</p>
            <p className="card-code">code: {articulus}</p>
            <p className="card-info">Lorem ipsum dolor sit amet, con
                adipiscing elit, sed diam nonu. </p>
            <div className="card-details" >
                <p>price: <span className="card-price">${price}</span></p>
                <div className="card-buttons">
                    <Button text={<i className="fas fa-times"> </i>} onClick={removeFromCartBtn}/>
                </div>
            </div>
        </div>
    );

    function removeFromCartBtn(){
        document.getElementById("modal-good-title").innerText = `${title}?`;
        document.getElementById("modal-good-title").dataset.id = `${id}`;
        document.getElementById("modal-img").src = `${url}100/100`;
        document.getElementById(modalId).classList.remove("modal-hide");
    }

}

export default CardAtCart;

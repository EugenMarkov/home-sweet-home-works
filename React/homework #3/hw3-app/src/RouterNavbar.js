import { Route, Switch  } from 'react-router-dom';
import React from 'react';
import Goods from './components/Goods/Goods';
import GoodsFavourites from "./components/GoodsFavourites/GoodsFavourites";
import Cart from "./components/Cart/Cart";


function Router() {
    return (
        <Switch>
            <Route exact path='/' component={() => <Goods/>}/> ;
            <Route path='/favourite' component={() => <GoodsFavourites/>}/>
            <Route path='/cart' component={() => <Cart/>}/>;
        </Switch>
    )
};

export default Router;

import React, {Component} from 'react';
import { connect } from "react-redux";
import { goodsFetchData } from './actions/goods';

import Nav from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import Router from "./RouterNavbar.js";

class App extends Component {
  componentDidMount() {
    this.props.fetchData('./goods.json');
  }


  render() {
      const goods = this.props.goods;
    return (
        <div>
            <Nav allGoods={goods}/>
            <Router goods={goods} />
            <Footer/>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    goods: state.goodsReducer.goods
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: url => dispatch( goodsFetchData(url)),
    // setFavGoods: (goods) => dispatch( setFavGoods(goods)),
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(App);

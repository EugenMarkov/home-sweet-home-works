const initialState = {
    goods: [],

};


function  goodsReducer(state = initialState, action) {
    switch (action.type) {
        case  "GOODS_FETCH_DATA_SUCCESS":
            return  {...state, goods: action.payload};
        case  "SET_FAV_GOODS":
            return  {...state, favGoods: action.payload};
        default:
            return state
    }
}

export default  goodsReducer;

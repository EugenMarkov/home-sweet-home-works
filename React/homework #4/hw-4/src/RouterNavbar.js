import { Route, Switch  } from 'react-router-dom';
import React from 'react';
import Goods from './components/Goods/Goods';
import GoodsFavourites from "./components/GoodsFavourites/GoodsFavourites";
import Cart from "./components/Cart/Cart";


function Router(props) {
    const { goods } = props;
    return (
        <Switch>
            <Route exact path='/' component={() => <Goods goods={goods}/>}/> ;
            <Route path='/favourite' component={() => <GoodsFavourites goods={goods}/>}/>
            <Route path='/cart' component={() => <Cart goods={goods}/>}/>;
        </Switch>
    )
};

export default Router;

export function goodsFetchData(url) {
    return(dispatch) => fetch(url)
        .then(response => {
           if(!response.ok) {
              throw new Error(response.statusText)
           } return response;
        })
        .then(response => response.json())
        .then(goods => dispatch(goodsFetchDataSuccess(goods)))
}

function goodsFetchDataSuccess(goods) {
    return {
        type: "GOODS_FETCH_DATA_SUCCESS",
        payload: goods
    }
}


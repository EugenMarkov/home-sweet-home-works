import React from 'react';
import CardFavourite from "../CardFavourite/CardFavourite";

function GoodsFavourites(props) {
    // const [goodsFavourites, setGoodsFavourites] = useState([]);
    // getGoodsFavourites(props.goods);

    const { goods=[] } = props;
    const goodsFavourites = goods.filter(good => localStorage.getItem(`Favourite${good.articulus}`));

    return(
        <div>
            <p className="goods-list-title">YOUR FAVOURITES TOURS</p>
            <div className="goods-list" id="favourites-goods-list">
                {goodsFavourites.map(item => {
                    return (
                        <CardFavourite id={item.articulus} key={item.url} title={item.title} price={item.price} url={item.url}
                                       articulus={item.articulus}
                        />)
                  })}
            </div>
        </div>
    );
}

export default GoodsFavourites;

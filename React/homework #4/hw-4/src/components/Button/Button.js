import React from 'react';
import PropTypes from 'prop-types';
import ButtonStyled from './ButtonStyled';

function Button(props) {
    const { backgroundColor, text, onClick, stylization } = props;

    return(
        <ButtonStyled className={stylization} style={{backgroundColor: backgroundColor}} onClick={onClick}>{text}</ButtonStyled>
    )
}

Button.defaultProps = {
    backgroundColor: "#1e1e20",
};

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text:  PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    onClick: PropTypes.func,
    stylization: PropTypes.string
};

export default Button;

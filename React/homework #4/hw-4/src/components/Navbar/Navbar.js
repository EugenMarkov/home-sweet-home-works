import React from 'react';
import {Link} from 'react-router-dom';

function Nav(){

        return (
        <div className="navbar">
            <Link className="navbar-link" to="/" >HOME PAGE</Link>
            <Link className="navbar-link" to="/favourite">FAVOURITE TOURS</Link>
            <Link className="navbar-link" to="/cart">CART  <span>
            </span></Link>
        </div>
        )
}

export default Nav;

import React, { useState } from "react";
import Button from '../Button/Button.js';
import Modal from "../Modal/Modal";

function CardAtCart (props){
    const { id, title, price, url, articulus } = props;
    const [isAddedToCart, setAddedToCart] = useState(true);
    const [showModalRemoveFromCart, setShowModalRemoveFromCart] = useState(false);

    const handleShowModalRemoveFromCart = () => setShowModalRemoveFromCart(true);
    const handleCloseModalRemoveFromCartCart = () => setShowModalRemoveFromCart(false);

    return(
        isAddedToCart &&
        <div id={id} className="card">
            <img className="card-img" src={`${url}300/300`} alt=""/>
            <p className="card-title">{title}</p>
            <p className="card-code">code: {articulus}</p>
            <p className="card-info">Lorem ipsum dolor sit amet, con adipiscing elit, sed diam nonu.</p>
            <div className="card-details">
                <p>price: <span className="card-price">${price}</span></p>
                <div className="card-buttons">
                    <Button text={<i className="fas fa-times"> </i>} onClick={handleShowModalRemoveFromCart}/>
                </div>
            </div>
            {showModalRemoveFromCart ? (
                <Modal id={"modal-remove-good-from-cart"}
                       onClose={handleCloseModalRemoveFromCartCart}
                       header={"Do you want to remove good from cart?"}
                       text={"Are you sure you want to remove good from cart?"}
                       url={url}
                       actions={[{text: "OK", onClick: removeFromCartModalBtn},
                       {text: "CANCEL",
                           onClick: handleCloseModalRemoveFromCartCart}]}
                > <img id="modal-img" src={`${url}100/100`} alt=""/></Modal>) : null}
        </div>
    );

    function removeFromCartModalBtn() {
        setAddedToCart(!isAddedToCart);
        if(localStorage.getItem(`AddedToCart${id}`)){
            localStorage.removeItem(`AddedToCart${id}`);
        }
        handleCloseModalRemoveFromCartCart();
    }
}

export default CardAtCart;

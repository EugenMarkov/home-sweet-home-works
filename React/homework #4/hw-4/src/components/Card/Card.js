import React, { useState } from "react";
import Button from '../Button/Button.js';
import Modal from "../Modal/Modal";

function Card (props){
    const { id, title, price, url, articulus } = props;
    const [isFavourite, setFavourite] = useState(props.isFavourite);
    const [showModalAddToCart, setShowModalAddToCart] = useState(false);

    function handleShowModalAddToCart () {
        return setShowModalAddToCart(true)
    }

    const handleCloseModalAddToCart = () => setShowModalAddToCart(false);

    return(
        <div id={id} className="card">
            <img className="card-img" src={`${url}300/300`} alt=""/>
            <p className="card-title">{title}</p>
            <p className="card-code">code: {articulus}</p>
            <p className="card-info">Lorem ipsum dolor sit amet, con adipiscing elit, sed diam nonu.</p>
            <div className="card-details" >
                <p>price: <span className="card-price">${price}</span></p>
                <div className="card-buttons">
                    <Button stylization={isFavourite ? "card-add-to-favourite-btn" : ""}
                            text={<i className="fas fa-star"> </i>}
                            onClick={clickFavouriteBtn}/>
                    <Button text={"ADD TO CART"} onClick={handleShowModalAddToCart}/>
                </div>
            </div>
            {showModalAddToCart ? (
                <Modal id={"modal-add-to-cart"}
                       onClose={handleCloseModalAddToCart}
                       header={"Do you want to add to cart new good?"}
                       url={url}
                       text={`Are you sure you want to add to cart ${title}?`}
                       actions={[{text: "OK", onClick: clickModalAddToCartBtn},
                           {text: "CANCEL", onClick: handleCloseModalAddToCart}]}
                />) : null}
        </div>
    );

   function clickFavouriteBtn(){
       setFavourite(!isFavourite);
       if(localStorage.getItem(`Favourite${id}`)){
           localStorage.removeItem(`Favourite${id}`)
       } else {
           localStorage.setItem(`Favourite${id}`, JSON.stringify("Favourite") );
       }
    }

    function clickModalAddToCartBtn(){
            console.log(id);
            if(localStorage.getItem(`AddedToCart${id}`)){
                console.log("Good was added before");
            } else {
                localStorage.setItem(`AddedToCart${id}`, JSON.stringify("Added to Cart") );
            }
            console.log(localStorage.getItem(`AddedToCart${id}`));
            handleCloseModalAddToCart();
        }
}

export default Card;

import React from 'react';
import ReactDOM from 'react-dom';

import Button from "../Button/Button.js";
import PropTypes from 'prop-types';
import ModalStyled from "./ModalStyled";
import ModalCloseBtnStyled from "./ModalCloseBtnStyled";
import ModalTitleStyled from "./ModalTitleStyled";
import ModalTextStyled from "./ModalTextStyled";
import ModalActionsBtnsStyled from "./ModalActionsBtnsStyled";
import ModalContentStyled from "./ModalContentStyled";

const modalRoot = document.getElementById('modal-root');

function Modal (props) {
    const { id, header, closeButton, text, url, actions, onClose } = props;

    return  ReactDOM.createPortal(
        <div id={id}>
            <div className="modal-overlap"
                 onClick={onClose}> </div>
            <ModalStyled>
                {closeButton && (<ModalCloseBtnStyled onClick={onClose}> </ModalCloseBtnStyled>)}
                <ModalTitleStyled>{header}</ModalTitleStyled>
                <ModalContentStyled>
                    <ModalTextStyled id="modal-text" className="modal-text">{text}</ModalTextStyled>
                    <img id="modal-img" src={`${url}100/100`} alt=""/>
                    {props.children}
                </ModalContentStyled>
                <ModalActionsBtnsStyled>
                    {actions.map((item)=> {
                        return (<Button key={item.text} text={item.text} onClick={item.onClick}/>)
                    })}
                </ModalActionsBtnsStyled>
            </ModalStyled>
        </div>,
        modalRoot,
    )
}

Modal.defaultProps = {
    closeButton: true,
    actions: [{text: "OK"},
              {text: "CANCEL"}],
};

Modal.propTypes = {
    id:  PropTypes.string,
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.array,
    bgcBtn: PropTypes.string
};

export default Modal;

import styled from 'styled-components';

const ModalTextStyled = styled.p`
  height: 30px;
  font-size: 15px;line-height: 2;
  text-align: center;
  margin-bottom: 10px;
`;

export default ModalTextStyled;

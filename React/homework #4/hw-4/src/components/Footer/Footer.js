import React from 'react';

function Footer() {
    return(
        <div className="footer">
            <p>Eugen Markov Inc. | &copy; Copyright 2019</p>
        </div>
    )
}

export default Footer;

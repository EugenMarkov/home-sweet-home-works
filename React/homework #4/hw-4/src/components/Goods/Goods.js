import React from 'react';
import Card from "../Card/Card";

function Goods (props) {
    const { goods } =  props;

        return (
            <div>
                <p className="goods-list-title">OUR BEST TOURS</p>
                <div className="goods-list" id="goods-list">
                    {goods.map(item => {
                        const idx = item.articulus;
                        const favouriteGood = localStorage.getItem(`Favourite${idx}`);
                        const favouriteGoodToBoolean = Boolean(favouriteGood);
                        const addedToCartGood = localStorage.getItem(`AddedToCart${idx}`);
                        const addedToCartGoodToBoolean = Boolean(addedToCartGood);
                        return (
                                <Card id={idx} key={item.url} title={item.title} price={item.price} url={item.url}
                                      articulus={item.articulus} isFavourite={favouriteGoodToBoolean}
                                      addToCart={addedToCartGoodToBoolean}
                                />
                        )
                    })}
                </div>
            </div>
        );
}

export default Goods;

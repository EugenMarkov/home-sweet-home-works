import React from 'react';
import CardAtCart from "../CardAtCart/CardAtCart";

function Cart(props) {
    const { goods=[] } = props;
    const goodsAtCart = goods.filter(good => localStorage.getItem(`AddedToCart${good.articulus}`));

    return (
        <div>
            <p className="goods-list-title">TOURS AT CART</p>
            <div className="goods-list" id="goods-at-cart-list">
                {goodsAtCart.map(item => {
                        return (
                            <CardAtCart id={item.articulus} key={item.url} title={item.title} price={item.price} url={item.url}
                                  articulus={item.articulus}
                            />
                        )
                })}
            </div>

        </div>
    );

}

export default Cart;

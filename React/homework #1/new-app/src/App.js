import React from 'react';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

function App() {
  return (
    <div>
        <div id="buttons">
            <Button text={"Open first modal"} backgroundColor={"red"}
                    onClick={ ()=> document.getElementById("modal-delete").classList.remove("modal-hide")}/>
            <Button text={"Open second modal"} backgroundColor={"blue"}
                    onClick={ ()=> document.getElementById("modal-create").classList.remove("modal-hide")}/>
        </div>
        <div id="modal-container">
            <Modal id={"modal-delete"} header={"Do you want to delete this file?"}
                   text={"Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"}
            />
            <Modal id={"modal-create"} header={"Do you want to create new file?"}
                   text={"Are you sure you want to create new file?"}
                   actions={[<Button key={"0"} text={"YES"} backgroundColor={"indigo"}/>,
                       <Button key={"1"} text={"NO"} backgroundColor={"indigo"}/>]}
            />
        </div>
    </div>
  );
}

export default App;

import React, { Component} from 'react';
import PropTypes from 'prop-types';
import ButtonStyled from './ButtonStyled';

class Button extends Component {
    constructor(props){
        super(props);
        this.backgroundColor = props.backgroundColor;
        this.text = props.text;
        this.onClick = props.onClick;
    }

    render() {
        return(
                <ButtonStyled style={{backgroundColor: this.backgroundColor}}
                        onClick={this.onClick}
                >{this.text}</ButtonStyled>
            )
    }
}

Button.defaultProps = {
    backgroundColor: "#b3382c"
};

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
};

export default Button;

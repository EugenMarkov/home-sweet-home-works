import styled from 'styled-components';

const ButtonStyled = styled.button`
margin: 10px;
padding: 15px;
border-radius: 5px;
border: none;
color: white;
cursor: pointer;
`;

export default ButtonStyled;

import styled from 'styled-components';

const ModalCloseBtnStyled = styled.div`
 position: absolute;
  right: 10px;
  top: 10px;
  width: 30px;
  height: 30px;
  border: none;
  color: white;
  cursor: pointer;
  
  &:hover {
    opacity: 1;
  }
  
  &:before{
  position: absolute;
  right: 15px;
  top: 5px;
  content: '';
  height: 22px;
  width: 2px;
  background-color: white;
  transform: rotate(45deg);
  }
  
  &:after {
  position: absolute;
  right: 15px;
  top: 5px;
  content: '';
  height: 22px;
  width: 2px;
  background-color: white;
  transform: rotate(-45deg);
}
`;

export default ModalCloseBtnStyled;
import React, { Component } from 'react';
import Button from "../Button/Button.js";
import PropTypes from 'prop-types';
import ModalStyled from "./ModalStyled";
import ModalCloseBtnStyled from "./ModalCloseBtnStyled";
import ModalTitleStyled from "./ModalTitleStyled";
import ModalTextStyled from "./ModalTextStyled";
import ModalActionsBtnsStyled from "./ModalActionsBtnsStyled";

class Modal extends Component {
    constructor(props) {
        super(props);
        this.id = props.id;
        this.header = props.header;
        this.closeButton = props.closeButton;
        this.text = props.text;
        this.actions = props.actions;
    }
        render(){
            const closeBtn = this.closeButton ?
                <ModalCloseBtnStyled onClick={()=> document.getElementById(this.id).classList.add("modal-hide")}>
                    </ModalCloseBtnStyled> : null;
            return (
                <div className="modal-hide" id={this.id}>
                    <div className="modal-overlap"
                         onClick={()=> document.getElementById(this.id).classList.add("modal-hide")}> </div>
                    <ModalStyled>
                        {closeBtn}
                        <ModalTitleStyled>{this.header}</ModalTitleStyled>
                        <ModalTextStyled className="modal-text">{this.text}</ModalTextStyled>
                        <ModalActionsBtnsStyled>
                            {this.actions.map((item)=> {
                                return item
                            })  }
                        </ModalActionsBtnsStyled>
                    </ModalStyled>
                </div>

            )
    }

}

Modal.defaultProps = {
    closeButton: true,
    actions: [<Button key={"0"} text={"Ok"}/>, <Button key={"1"} text={"Cancel"}/>],
};

Modal.propTypes = {
    id:  PropTypes.string,
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.array,
    bgcBtn: PropTypes.string
};

export default Modal;

